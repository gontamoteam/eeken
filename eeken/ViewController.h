//
//  ViewController.h
//  eeken
//
//  Created by gontamo on 13/05/19.
//  Copyright (c) 2013年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioServices.h>

SystemSoundID soundID,soundID2,soundID3,soundID4;

@interface ViewController : UIViewController
{
    IBOutlet UILabel *label;
    IBOutlet UILabel *lbl;
    IBOutlet UILabel *scorelabel;
    NSTimer *timer;
    int nokorijikan;
    IBOutlet UIButton *moveButtonnabe;
    IBOutlet UIButton *moveButtongomibako;
    IBOutlet UIButton *enemy;
    int count;
    int guzai;
    int score;
}
@property(nonatomic,retain) UILabel *lbl;

-(void)onTimer:(NSTimer*)timer;
-(IBAction) cButtonPressed:(id) sender;
-(IBAction) start_down:(id)sender;
-(IBAction) moveButtonnabePressed;
-(IBAction) moveButtongomibakoPressed;
-(void)completion:(id)sender;
-(void)grow:(id)sender;
@end
