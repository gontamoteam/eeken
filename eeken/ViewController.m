//
//  ViewController.m
//  eeken
//
//  Created by gontamo on 13/05/19.
//  Copyright (c) 2013年 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@end

@implementation ViewController
-(void) viewDidLoad{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
//    UIImage *backgroundImage = [[UIImage imageNamed:@"background.jpg"] retain];
//    UIImage *backgroundImage = [[UIImage alloc] initWithContentsOfFile:
//                                [[NSBundle mainBundle] pathForResource:@"background" ofType:@"jpg"]];
//    [backgroundImage autorelease];
//    self.view.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
//    [backgroundImage release];
    
    count = 0;
    score = 0;
    timer = [NSTimer scheduledTimerWithTimeInterval:(0.001)
                                             target:self
                                           selector:@selector(onTimer:)
                                           userInfo:nil
                                            repeats:YES];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"mizu" ofType:@"mp3"];
    NSURL *url = [NSURL fileURLWithPath:path];
    AudioServicesCreateSystemSoundID((CFURLRef)url, &soundID);
    NSString *path2 = [[NSBundle mainBundle] pathForResource:@"yukkuri" ofType:@"mp3"];
    NSURL *url2 = [NSURL fileURLWithPath:path2];
    AudioServicesCreateSystemSoundID((CFURLRef)url2, &soundID2);
    NSString *path3 = [[NSBundle mainBundle] pathForResource:@"bomb" ofType:@"mp3"];
    NSURL *url3 = [NSURL fileURLWithPath:path3];
    AudioServicesCreateSystemSoundID((CFURLRef)url3, &soundID3);
    NSString *path4 = [[NSBundle mainBundle] pathForResource:@"gomi" ofType:@"mp3"];
    NSURL *url4 = [NSURL fileURLWithPath:path4];
    AudioServicesCreateSystemSoundID((CFURLRef)url4, &soundID4);
}

//タイマー
@synthesize lbl;
NSDate *stdate;
BOOL timeflg = FALSE;
BOOL nokorijikanflg = TRUE;
-(void)onTimer:(NSTimer *)timer
{
    if(timeflg)
    {
        NSDate *now = [NSDate date];
        nokorijikan = 10;
        nokorijikan = 10-[now timeIntervalSinceDate:stdate];
        self.lbl.text = [NSString stringWithFormat:@"%.1f",10-[now timeIntervalSinceDate:stdate]];
        if([now timeIntervalSinceDate:stdate]>=10)
        {
            self.lbl.text = [NSString stringWithFormat:@"0.0"];
            nokorijikanflg = FALSE;
        }
    }

}

//タイマー開始
-(IBAction) start_down:(id)sender
{
    if(stdate==0)
    {
    timeflg = TRUE;
    stdate = [NSDate date];
    [stdate retain];
    }
}

//clearボタンを押したときの動作
-(IBAction) cButtonPressed:(id) sender
{
    count = 0;
    stdate = 0;
    guzai = 0;
    [moveButtonnabe setImage : [ UIImage imageNamed : @"gohan.gif" ] forState : UIControlStateNormal];
    label.text = [NSString stringWithFormat:@"%d", count];
    timeflg = FALSE;
    nokorijikanflg = TRUE;
    self.lbl.text = @"10.0";
}

//左下のボタンを押したときの動作
-(IBAction) moveButtongomibakoPressed
{
    if(nokorijikanflg){
        [UIView animateWithDuration:0.15f
                     animations:^{
                         int xPos = 40;
                         int yPos = 410;
                         moveButtongomibako.center = CGPointMake(xPos,yPos);
                         AudioServicesPlaySystemSound(soundID4);
                     }
                     completion:^(BOOL finished){
                         [self completion:nil];
                     }
     ];
    }
}

//中央と右下のボタンを押したときの動作
-(IBAction) moveButtonnabePressed
{
    if(nokorijikanflg){
        [UIView animateWithDuration:0.15f
                         animations:^
         {
             int xPos = 160;
             int yPos = 240;
             moveButtonnabe.center = CGPointMake(xPos,yPos);
             if(guzai==0)
             {
                 AudioServicesPlaySystemSound(soundID);
                 count++;
             }
             if(guzai==1)
             {
                 AudioServicesPlaySystemSound(soundID2);
                 count--;
             }
             if(guzai==2)
             {
                 AudioServicesPlaySystemSound(soundID3);
                 count = 0;
             }
             label.text = [NSString stringWithFormat:@"%d", count];
         }
                         completion:^(BOOL finished)
         {
             [self grow:nil];
         }
         ];
    }
}

//中央ボタンの画像を変える
- (void)grow:(id)sender
{
    if(count<=1)
    {
        [enemy setImage : [ UIImage imageNamed : @"enemy1-1.png" ] forState : UIControlStateNormal];
    }
    if(2<=count&&count<=4)
    {
        [enemy setImage : [ UIImage imageNamed : @"enemy1-2.png" ] forState : UIControlStateNormal];
    }
    if(5<=count)
    {
        [enemy setImage : [ UIImage imageNamed : @"enemy1-3.png" ] forState : UIControlStateNormal];
    }
    [self completion:nil];
}

//右下にボタンの位置を戻して画像を変える
- (void)completion:(id)sender
{
    int xPos = 270;
    int yPos = 410;
    moveButtonnabe.center = CGPointMake(xPos,yPos);
    guzai = arc4random() % 3;
    if(guzai==0){
        [moveButtonnabe setImage : [ UIImage imageNamed : @"gohan.gif" ] forState : UIControlStateNormal];
    }
    if(guzai==1){
        [moveButtonnabe setImage:[ UIImage imageNamed: @"togarashi.gif"] forState: UIControlStateNormal];
    }
    if(guzai==2){
        [moveButtonnabe setImage : [ UIImage imageNamed : @"bomb.png" ] forState : UIControlStateNormal];
    }
}

//スコアを決める
- (void)score:(id)sender
{
    if(nokorijikan==0){
        if(count<=1)
        {
            score += 1;
        }
        if(2<=count&&count<=4)
        {
            score += 2;
        }
        if(5<=count)
        {
            score += 3;
        }
        scorelabel.text = [NSString stringWithFormat:@"%d", score];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}
@end
